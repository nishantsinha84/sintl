DROP TABLE IF EXISTS sintldata_report.test_summary;
CREATE TABLE sintldata_report.test_summary (
	region VARCHAR(20) NOT NULL
	,org_id INTEGER NOT NULL
	,school_name VARCHAR(255) NOT NULL
	,year INTEGER
	,term VARCHAR(20)
	,grade VARCHAR(20)
	,test_count INTEGER
	);
