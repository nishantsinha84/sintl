
DROP TABLE IF EXISTS sintldata_report.scorecard;
CREATE TABLE sintldata_report.scorecard (
	region VARCHAR(20)
	,year INTEGER
	,org_id INTEGER
	,school_name VARCHAR(255)
	,lp_count INTEGER
	,lpl_count INTEGER
	,total_licenses INTEGER
	,school_size VARCHAR(15)
	,product_coverage VARCHAR(15)
	,continuity VARCHAR(15)
	,size_factor DECIMAL(38,2)
	,continuity_factor DECIMAL(38,2)
	,coverage_factor DECIMAL(38,2)
	,rating DECIMAL(38,2)
	,scorecard VARCHAR(15)
	);