DROP TABLE IF EXISTS sintldata_report.quiz_summary;
CREATE TABLE sintldata_report.quiz_summary
(region VARCHAR(20) NOT NULL
    ,org_id integer
    ,id INTEGER 
	,school_name VARCHAR(255)
	,year INTEGER
	,term VARCHAR(20)
	,grade VARCHAR(20)
	,pass_count INTEGER 
	,fail_count INTEGER 
	,quiz_count INTEGER 
	,word_count INTEGER 
	);
