DROP TABLE IF EXISTS sintldata_stg.litpro_sri_issued_tests;

CREATE TABLE sintldata_stg.litpro_sri_issued_tests
(
   id                        integer,
   created                   timestamp,
   current_question          integer,
   effective_time            timestamp,
   end_lexile                integer,
   end_sigma                 numeric(10,6),
   modified                  timestamp,
   school_year               integer,
   sequencer_state           integer,
   start_lexile              integer,
   start_lexile_description  varchar(256),
   start_sigma               numeric(10,6),
   state_description         varchar(256),
   sum_auto_skipped          integer,
   sum_correct               integer,
   sum_incorrect             integer,
   sum_skipped               integer,
   test_state                integer,
   time_elapsed_seconds      integer,
   time_end                  timestamp,
   time_start                timestamp,
   grade_id                  integer,
   user_id                   integer,
   imported_sri_test         integer
);
