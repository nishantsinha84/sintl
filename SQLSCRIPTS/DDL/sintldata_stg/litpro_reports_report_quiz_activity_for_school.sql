DROP TABLE IF EXISTS sintldata_stg.litpro_reports_report_quiz_activity_for_school;

CREATE TABLE sintldata_stg.litpro_reports_report_quiz_activity_for_school
(
   id                      bigint,
   created                 timestamp,
   modified                timestamp,
   report_date             date,
   school_year             integer,
   school_id               integer,
   lf_class_id             integer,
   student_id              integer,
   grade_code              varchar(2),
   src_activity_id         integer,
   quiz_name               varchar(200),
   quiz_date               date,
   quiz_questions_correct  integer,
   quiz_questions_issued   integer,
   points                  integer,
   points_teacher          integer,
   lexile                  integer,
   word_count              integer,
   grade_id                integer,
   passed                  integer
);
