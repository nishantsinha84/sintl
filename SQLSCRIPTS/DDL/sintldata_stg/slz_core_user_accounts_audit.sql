DROP TABLE IF EXISTS sintldata_stg.slz_core_user_accounts_audit;	
	
CREATE TABLE sintldata_stg.slz_core_user_accounts_audit (
	id INTEGER
	,user_id VARCHAR(255)
	,user_type VARCHAR(50)
	,STATUS VARCHAR(20)
	,created_at TIMESTAMP
	,updated_at TIMESTAMP
	,deleted VARCHAR(10)
	);
