DROP TABLE IF EXISTS sintldata_stg.litpro_src_activities;
CREATE TABLE sintldata_stg.litpro_src_activities (
	id INTEGER
	,activity_date TIMESTAMP 
	,book_title VARCHAR(200) 
	,created TIMESTAMP 
	,modified TIMESTAMP 
	,passed INTEGER 
	,points INTEGER 
	,points_teacher INTEGER 
	,quiz_questions_correct INTEGER 
	,quiz_questions_issued INTEGER 
	,school_year INTEGER 
	,sequence_num INTEGER 
	,title VARCHAR(200) 
	,type_id INTEGER 
	,book_id INTEGER 
	,grade_id INTEGER 
	,src_quiz_id INTEGER 
	,student_user_id INTEGER 
	,teacher_user_id INTEGER 
	,imported INTEGER
	,status INTEGER 
	);

