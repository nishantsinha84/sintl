DROP TABLE IF EXISTS sintldata_stg.slz_core_organizations;

CREATE TABLE sintldata_stg.slz_core_organizations
(
   id                    integer,
   ucn                   integer,
   siteid                varchar(255),
   name                  varchar(255),
   org_guid              varchar(255),
   org_uuid              varchar(255),
   address1              varchar(255),
   address2              varchar(255),
   address3              varchar(255),
   country               varchar(255),
   postal_code           varchar(255),
   parent_id             integer,
   region_id             integer,
   country_id            integer,
   grade_system_id       integer,
   source_user_id        integer,
   time_zone_id          integer,
   test_org              integer,
   srii_litpro_flag      integer,
   admin_flag            integer,
   perpetual_flag        integer,
   show_basic_aaiz       integer,
   slz_basic_cdn_assets  integer,
   reorg_timestamp       timestamp,
   created_at            timestamp,
   updated_at            timestamp,
   deleted               varchar(255),
   customer_group        varchar(30),
   customer_type         varchar(30),
   organization_type     varchar(30),
   customer_name         varchar(255),
   customer_position     varchar(50),
   customer_address      varchar(255),
   customer_email        varchar(255),
   customer_phone        varchar(20),
   customer_fax          varchar(20),
   customer_opt_in       integer,
   contact_mode          varchar(20),
   enabled               integer,
   notes                 varchar(10000),
   student_count         integer,
   active_student_count  integer,
   teacher_count         integer,
   active_teacher_count  integer,
   admin_count           integer,
   active_admin_count    integer,
   admin_area_id         integer,
   country_locality_id   integer,
   sso_enabled           integer,
   org_source_id         varchar(255)
);
