DROP TABLE IF EXISTS sintldata_stg.slz_custportal_order_line_license_tracking;

CREATE TABLE sintldata_stg.slz_custportal_order_line_license_tracking (
		id INTEGER 
		,order_line_id INTEGER 
		,license_count INTEGER 
		,sales_amount NUMERIC(17, 2) 
		,created_at TIMESTAMP
		,updated_at TIMESTAMP
		,deleted VARCHAR(10)
		);
