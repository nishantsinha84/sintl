DROP TABLE IF EXISTS sintldata_stg.slz_core_order_entitlements;

CREATE TABLE sintldata_stg.slz_core_order_entitlements
(
   id                       integer,
   order_number             varchar(255),
   order_line_id            integer,
   product_id               integer,
   organization_id          integer,
   subscription_start_date  timestamp,
   subscription_end_date    timestamp,
   grace_period_in_days     integer,
   grace_period_start_date  timestamp,
   grace_period_end_date    timestamp,
   is_active                integer,
   renewal_entitlement_id   integer,
   num_of_days              integer,
   num_of_seats             integer,
   created_at               timestamp,
   updated_at               timestamp,
   deleted                  varchar(10),
   order_id                 integer,
   order_type               varchar(50),
   license_count            integer,
   language_code            varchar(50)
);
