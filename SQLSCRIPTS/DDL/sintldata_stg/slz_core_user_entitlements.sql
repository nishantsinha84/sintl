DROP TABLE IF EXISTS sintldata_stg.slz_core_user_entitlements ;

CREATE TABLE sintldata_stg.slz_core_user_entitlements (
	id INTEGER
	,user_id INTEGER
	,product_id INTEGER
	,order_reference INTEGER
	,created_at TIMESTAMP
	,updated_at TIMESTAMP
	,deleted VARCHAR(10)
	);
