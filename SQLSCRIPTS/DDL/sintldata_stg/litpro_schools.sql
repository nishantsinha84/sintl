DROP TABLE IF EXISTS sintldata_stg.litpro_schools;
CREATE TABLE sintldata_stg.litpro_schools (
	id INTEGER
	,created TIMESTAMP
	,modified TIMESTAMP
	,slz_id VARCHAR(200)
	,benchmark_id INTEGER
	,slz_url_id BIGINT
	,test_account SMALLINT
	,country_id INTEGER
	);
