DROP TABLE IF EXISTS sintldata_stg.slz_core_products;

CREATE TABLE sintldata_stg.slz_core_products
(
   id                integer,
   code              varchar(255),
   description       varchar(255),
   isbn              varchar(255),
   is_offline        integer,
   is_roster         integer,
   application_id    integer,
   product_group_id  integer,
   created_at        timestamp,
   updated_at        timestamp,
   deleted           varchar(10),
   display_name      varchar(255)
);
