DROP TABLE IF EXISTS sintldata_stg.slz_custportal_orders;

CREATE TABLE sintldata_stg.slz_custportal_orders (
	id INTEGER
	,order_number VARCHAR(100)
	,order_date TIMESTAMP
	,invoice_number VARCHAR(100)
	,po_number VARCHAR(100)
	,source_system_name VARCHAR(100)
	,created_by_user_id INTEGER
	,created_at TIMESTAMP
	,updated_at TIMESTAMP
	,deleted VARCHAR(10)
	,siteid VARCHAR(50) NOT NULL
	);
