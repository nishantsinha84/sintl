DROP TABLE IF EXISTS sintldata_stg.litpro_lf_classes;
CREATE TABLE sintldata_stg.litpro_lf_classes (
	id INTEGER
	,benchmark_updated_at TIMESTAMP
	,created TIMESTAMP
	,modified TIMESTAMP
	,slz_id VARCHAR(200)
	,benchmark_id INTEGER
	,school_id INTEGER
	);

