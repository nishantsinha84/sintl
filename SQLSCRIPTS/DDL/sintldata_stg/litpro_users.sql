DROP TABLE IF EXISTS sintldata_stg.litpro_users;
CREATE TABLE sintldata_stg.litpro_users (
	id INTEGER
	,avg_quiz_lexile INTEGER
	,avg_quiz_score INTEGER
	,created TIMESTAMP
	,lexile INTEGER
	,lexile_fully_computed SMALLINT
	,lexile_updated TIMESTAMP
	,modified TIMESTAMP
	,num_quiz_attempted INTEGER
	,num_quiz_passed INTEGER
	,points_earned INTEGER
	,slz_id VARCHAR(200)
	,timezone_offset INTEGER
	,words_read INTEGER
	,school_id INTEGER
	,school_group_id INTEGER
	);
