DROP TABLE IF EXISTS sintldata_stg.slz_custportal_order_lines;

CREATE TABLE sintldata_stg.slz_custportal_order_lines
(
   id                        integer,
   order_id                  integer,
   price                     numeric(17,2),
   sales_rep_user_id         integer,
   inside_sales_rep_user_id  integer,
   distributor_user_id       integer,
   license_count             integer,
   subscription_start_date   timestamp,
   subscription_end_date     timestamp,
   status                    varchar(10),
   grace_period_days         integer,
   order_type_id             integer,
   created_at                timestamp,
   updated_at                timestamp,
   deleted                   varchar(10),
   num_of_days               integer,
   num_of_seats              integer,
   currency_code             varchar(45),
   product_code              varchar(50),
   language_code             varchar(50)
);
