DROP TABLE IF EXISTS sintldata_stg.slz_portal_application_usage;
	
CREATE TABLE sintldata_stg.slz_portal_application_usage (
		id INTEGER
		,user_uuid VARCHAR(255)
		,user_type VARCHAR(255)
		,app_code VARCHAR(255)
		,usage_minute INTEGER
		,no_of_session INTEGER
		,org_id INTEGER
		,created_at TIMESTAMP 
		,updated_at TIMESTAMP
		,deleted VARCHAR(10)
		,access_token VARCHAR(255)
		);
