DROP TABLE IF EXISTS sintldata_stg.litpro_grades;

CREATE TABLE sintldata_stg.litpro_grades
(
   id                      integer,
   code                    varchar(2),
   lexile_above            integer,
   lexile_below            integer,
   lexile_far_above        integer,
   lexile_far_below        integer,
   lexile_growth_estimate  integer,
   lexile_hilo_pool        integer,
   lexile_on_level         integer,
   lexile_practice         integer,
   proficiency_above       integer,
   proficiency_below       integer,
   proficiency_far_below   integer,
   proficiency_on_level    integer,
   interest_level_id       integer
);
