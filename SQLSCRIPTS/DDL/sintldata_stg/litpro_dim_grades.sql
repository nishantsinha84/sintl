DROP TABLE IF EXISTS sintldata_stg.litpro_dim_grades;
CREATE TABLE sintldata_stg.litpro_dim_grades (
	id INTEGER
	,local_grade_name VARCHAR(255)
	,scholastic_grade_code VARCHAR(255)
	,sequence_num INTEGER
	,slz_school_id VARCHAR(255)
	,slz_timestamp TIMESTAMP
	,status VARCHAR(1)
	);