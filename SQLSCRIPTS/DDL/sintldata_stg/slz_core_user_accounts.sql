DROP TABLE IF EXISTS sintldata_stg.slz_core_user_accounts;

CREATE TABLE sintldata_stg.slz_core_user_accounts
(
   id                     integer,
   uuid                   varchar(255),
   district_user_id       varchar(255),
   username               varchar(255),
   password               varchar(255),
   first_name             varchar(255),
   last_name              varchar(255),
   type                   varchar(255),
   email                  varchar(255),
   lexile_level           integer,
   lexile_source          varchar(255),
   enabled                integer,
   organization_id        integer,
   organization_group_id  integer,
   grade_id               integer,
   created_at             timestamp,
   updated_at             timestamp,
   deleted                varchar(10),
   phone                  varchar(20),
   primary_admin          integer,
   avail_zone_pref        integer,
   alerts_updates_pref    integer,
   upcoming_offers_pref   integer,
   act_as_admin           integer,
   last_logged_in_at      timestamp,
   customer_role          varchar(20),
   last_modified_user_at  timestamp,
   see_last_logged_in     timestamp
);
