DROP TABLE IF EXISTS sintldata_wrk.quiz;
CREATE TABLE sintldata_wrk.quiz (
	region VARCHAR(20) NOT NULL
	,quiz_id INTEGER NOT NULL
	,school_id INTEGER NOT NULL
	,student_id integer
	,year INTEGER NOT NULL
	,quiz_date DATE
	,term varchar(10)
	,result  varchar(15)
	,word_count INTEGER
	,grade varchar(20)
	);

