DROP TABLE IF EXISTS sintldata_wrk.order_details;
CREATE table sintldata_wrk.order_details
(
 region varchar(10),
 order_line_id integer,
 organization_id integer,
 product_id integer,
 subscription_start_date timestamp,
 subscription_end_date timestamp,
 year varchar(4),
 order_type varchar(50),
 license_count integer
 );