DROP TABLE IF EXISTS sintldata_wrk.users;
create table  sintldata_wrk.users
(
 region varchar(10),
 id integer,
 first_name varchar (255),
 last_name varchar (255),
 type varchar (255),
 organization_id integer,
 deleted varchar (10),
 user_id integer,
 school_id integer
);