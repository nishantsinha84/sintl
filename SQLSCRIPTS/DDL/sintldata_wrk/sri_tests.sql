DROP TABLE IF EXISTS sintldata_wrk.sri_tests;
CREATE TABLE sintldata_wrk.sri_tests (
	region VARCHAR(20) NOT NULL
	,test_id INTEGER NOT NULL
	,user_id INTEGER NOT NULL
	,grade_id INTEGER NOT NULL
	,year INTEGER DEFAULT 0
	,created TIMESTAMP
	,start TIMESTAMP
	,finish TIMESTAMP
	);
