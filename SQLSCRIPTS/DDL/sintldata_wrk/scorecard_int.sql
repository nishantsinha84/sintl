
DROP TABLE IF EXISTS sintldata_wrk.scorecard_int;
CREATE TABLE sintldata_wrk.scorecard_int (
	region VARCHAR(20)
	,year INTEGER
	,org_id INTEGER
	,school_name VARCHAR(255)
	,lp_count_cur_yr INTEGER
	,lp_count_pre_yr INTEGER
	,lpl_count_cur_yr INTEGER
	,lpl_count_pre_yr INTEGER
	,total_licenses_cur_yr INTEGER
	,total_licenses_pre_yr INTEGER
	,school_size VARCHAR(15)
	,product_coverage VARCHAR(15)
	,continuity VARCHAR(15)
	,size_factor DECIMAL(38,2)
	,continuity_factor DECIMAL(38,2)
	,coverage_factor DECIMAL(38,2)
	,rating DECIMAL(38,2)
	,scorecard VARCHAR(15)
	);

