DROP TABLE IF EXISTS sintldata_wrk.products;
CREATE table sintldata_wrk.products
(
 region varchar(10),
 product_id integer,
 code varchar(255),
 description varchar(255),
 display_name  varchar(255),
 deleted varchar(10)
);