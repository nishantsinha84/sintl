TRUNCATE sintldata_report.scorecard;
INSERT INTO sintldata_report.scorecard
(
SELECT region
	,year
	,org_id
	,school_name
	,lp_count_cur_yr AS lp_count
	,lpl_count_cur_yr AS lpl_count
	,total_licenses_cur_yr AS total_licenses
	,school_size
	,product_coverage
	,continuity
	,size_factor
	,continuity_factor
	,coverage_factor
	,rating
	,scorecard
FROM sintldata_wrk.scorecard_int
WHERE year=2019
);