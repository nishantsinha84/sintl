TRUNCATE Table sintldata_report.test_summary;
Insert Into sintldata_report.test_summary
(Select  tgf.region,
        tgf.org_id,
        tgf.school_name,
        tgf.year,
        tgf.term,
        tgf.grade,
        count((tgf.test_id)) as test_count
        
FROM(
Select tg.region,
       tg.org_id,
       tg.school_name,
       tg.year,
       tg.test_id,
       case tg.month
       when tg.month IN(1,2,3)then 'Term 1'
       when tg.month IN(4,5,6) then 'Term 2'
       when tg.month IN(7,8,9) then 'Term 3'
       when tg.month IN(10,11,12) then 'Term 4'
       end as term,
       'Grade-'+gd.code as grade,
       '' as count
From(
select ou.region,
       ou.org_id,
       ou.school_name,
       Date_Part(Y,test.start) as year,
        extract(month from test.start)as month,
        test.grade_id,
        test.test_id,
        '' as term
        
from (Select 'Zone-4' as region,
        org.id as org_id,
        org.name as school_name,
        ur.id as user_id
        
From sintldata_wrk.organizations org
 Join sintldata_wrk.users ur on org.id=ur.organization_id )ou
 JOIN sintldata_wrk.sri_tests test ON ou.user_id=test.user_id) tg
LEFT JOIN sintldata_wrk.grades gd ON tg.grade_id=gd.id )tgf

Group by  tgf.region,
        tgf.org_id,
        tgf.school_name,
        tgf.year,
        tgf.term,
        tgf.grade
order by tgf.org_id,tgf.year,tgf.term,tgf.grade asc

);
