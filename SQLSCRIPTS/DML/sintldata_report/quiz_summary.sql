TRUNCATE TABLE sintldata_report.quiz_summary;

INSERT INTO sintldata_report.quiz_summary (
SELECT region
    ,org_id
    ,id
	,school_name
	,year
	,term
	,grade
	,sum(pass_count) AS pass_count
	,sum(fail_count) AS fail_count
	,sum(quiz_count) AS quiz_count
	,sum(word_count) AS word_count 
FROM sintldata_wrk.quiz_summary_int 
GROUP BY region
	,org_id
	,id
	,school_name
	,year
	,term
	,grade 
ORDER BY id
	,year
	,term
	,grade
	);
