TRUNCATE TABLE sintldata_report.license_trend;

INSERT INTO sintldata_report.license_trend(
	SELECT DISTINCT 'Zone-4' AS region
	,org_id 
	,org.name as organization_name
	,substring(created_at, 1, 4) AS year
	,user_type as Attribute
	,count(DISTINCT user_uuid) AS count 
 FROM sintldata_stg.slz_portal_application_usage App
 LEFT JOIN sintldata_wrk.organizations org ON app.org_id=org.id
 WHERE user_type IS NOT NULL 
 AND org.name IS NOT NULL
 GROUP BY org_id
  ,org.name
	,substring(created_at, 1, 4)
	,user_type
	);
	
INSERT INTO sintldata_report.license_trend(
Select 'Zone-4' AS region
        ,ord.organization_id
        ,org.name as  organization_name
        ,substring(ord.subscription_start_date, 1, 4) AS year
        ,case ord.product_id
       when '44' then 'LP License' 
       when '46' then'LPL License'
       END as Attribute
       ,sum(ord.license_count) as count
       
From sintldata_wrk.order_details ord
LEFT JOIN sintldata_wrk.organizations org ON ord.organization_id=org.id
GROUP BY ord.organization_id
        ,org.name 
        ,substring(ord.subscription_start_date, 1, 4)
        ,ord.product_id
       
);

	

