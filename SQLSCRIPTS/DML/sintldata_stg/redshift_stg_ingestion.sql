copy sintldata_stg.litpro_grades from 's3://sintl-data-analytics-07092019/litpro/grades.csv' iam_role 'arn:aws:iam::209874575205:role/sintl-data-analytics-07092019-role' 
timeformat 'YYYY-MM-DD HH:MI:SS' escape removequotes delimiter as ',' IGNOREHEADER 1 ACCEPTINVCHARS COMPUPDATE OFF STATUPDATE OFF;

copy sintldata_stg.litpro_sri_issued_tests from 's3://sintl-data-analytics-07092019/litpro/sri_issued_tests.csv' iam_role 'arn:aws:iam::209874575205:role/sintl-data-analytics-07092019-role' 
timeformat 'YYYY-MM-DD HH:MI:SS' escape removequotes delimiter as ',' IGNOREHEADER 1 ACCEPTINVCHARS COMPUPDATE OFF STATUPDATE OFF;

copy sintldata_stg.litpro_reports_report_quiz_activity_for_school from 's3://sintl-data-analytics-07092019/litpro_reports/report_quiz_activity_for_school.csv' iam_role 'arn:aws:iam::209874575205:role/sintl-data-analytics-07092019-role' 
timeformat 'YYYY-MM-DD HH:MI:SS' escape removequotes delimiter as ',' IGNOREHEADER 1 ACCEPTINVCHARS COMPUPDATE OFF STATUPDATE OFF;

copy sintldata_stg.slz_core_order_entitlements from 's3://sintl-data-analytics-07092019/slz_core/order_entitlements.csv' iam_role 'arn:aws:iam::209874575205:role/sintl-data-analytics-07092019-role' 
timeformat 'YYYY-MM-DD HH:MI:SS' escape removequotes delimiter as ',' IGNOREHEADER 1 ACCEPTINVCHARS COMPUPDATE OFF STATUPDATE OFF;

copy sintldata_stg.slz_core_organizations from 's3://sintl-data-analytics-07092019/slz_core/organizationsall.csv' iam_role 'arn:aws:iam::209874575205:role/sintl-data-analytics-07092019-role' 
timeformat 'YYYY-MM-DD HH:MI:SS' escape removequotes delimiter as '|' IGNOREHEADER 1 ACCEPTINVCHARS COMPUPDATE OFF STATUPDATE OFF;

copy sintldata_stg.slz_core_products from 's3://sintl-data-analytics-07092019/slz_core/products.csv' iam_role 'arn:aws:iam::209874575205:role/sintl-data-analytics-07092019-role' 
timeformat 'YYYY-MM-DD HH:MI:SS' escape removequotes delimiter as ',' IGNOREHEADER 1 ACCEPTINVCHARS COMPUPDATE OFF STATUPDATE OFF;

copy sintldata_stg.slz_core_user_accounts from 's3://sintl-data-analytics-07092019/slz_core/user_accounts.csv' iam_role 'arn:aws:iam::209874575205:role/sintl-data-analytics-07092019-role' 
timeformat 'YYYY-MM-DD HH:MI:SS' escape removequotes delimiter as ',' IGNOREHEADER 1 ACCEPTINVCHARS COMPUPDATE OFF STATUPDATE OFF;

copy sintldata_stg.slz_custportal_order_lines from 's3://sintl-data-analytics-07092019/slz_custportal/order_lines.csv' iam_role 'arn:aws:iam::209874575205:role/sintl-data-analytics-07092019-role' 
timeformat 'YYYY-MM-DD HH:MI:SS' escape removequotes delimiter as ',' IGNOREHEADER 1 ACCEPTINVCHARS COMPUPDATE OFF STATUPDATE OFF;

copy sintldata_wrk.active_schools_2019 from 's3://sintl-data-analytics-07092019/slz_core/active_schools_2019.csv' iam_role 'arn:aws:iam::209874575205:role/sintl-data-analytics-07092019-role' 
timeformat 'YYYY-MM-DD HH:MI:SS' escape removequotes delimiter as ',' IGNOREHEADER 1 ACCEPTINVCHARS COMPUPDATE OFF STATUPDATE OFF;

copy sintldata_stg.slz_custportal_order_line_license_tracking from 's3://sintl-data-analytics-07092019/slz_custportal/order_line_license_tracking.csv' iam_role 'arn:aws:iam::209874575205:role/sintl-data-analytics-07092019-role' 
timeformat 'YYYY-MM-DD HH:MI:SS' escape removequotes delimiter as ',' IGNOREHEADER 1 ACCEPTINVCHARS COMPUPDATE OFF STATUPDATE OFF;

copy sintldata_stg.slz_core_user_entitlements from 's3://sintl-data-analytics-07092019/slz_core/user_entitlements.csv' iam_role 'arn:aws:iam::209874575205:role/sintl-data-analytics-07092019-role' 
timeformat 'YYYY-MM-DD HH:MI:SS' escape removequotes delimiter as ',' IGNOREHEADER 1 ACCEPTINVCHARS COMPUPDATE OFF STATUPDATE OFF;

copy sintldata_stg.slz_custportal_orders from 's3://sintl-data-analytics-07092019/slz_custportal/orders_fixed.csv' iam_role 'arn:aws:iam::209874575205:role/sintl-data-analytics-07092019-role'
timeformat 'YYYY-MM-DD HH:MI:SS' escape removequotes delimiter as ',' IGNOREHEADER 1 ACCEPTINVCHARS COMPUPDATE OFF STATUPDATE OFF;

copy sintldata_stg.litpro_src_activities from 's3://sintl-data-analytics-07092019/litpro/src_activities_fixed.csv' iam_role 'arn:aws:iam::209874575205:role/sintl-data-analytics-07092019-role' 
timeformat 'YYYY-MM-DD HH:MI:SS' escape removequotes delimiter as ',' IGNOREHEADER 1 ACCEPTINVCHARS COMPUPDATE OFF STATUPDATE OFF;

copy sintldata_stg.litpro_dim_grades from 's3://sintl-data-analytics-07092019/litpro/dim_grades.csv' iam_role 'arn:aws:iam::209874575205:role/sintl-data-analytics-07092019-role' 
timeformat 'YYYY-MM-DD HH:MI:SS' escape removequotes delimiter as ',' IGNOREHEADER 1 ACCEPTINVCHARS COMPUPDATE OFF STATUPDATE OFF;

copy sintldata_stg.litpro_schools from 's3://sintl-data-analytics-07092019/litpro/schools.csv' iam_role 'arn:aws:iam::209874575205:role/sintl-data-analytics-07092019-role' 
timeformat 'YYYY-MM-DD HH:MI:SS' escape removequotes delimiter as ',' IGNOREHEADER 1 ACCEPTINVCHARS COMPUPDATE OFF STATUPDATE OFF;

copy sintldata_stg.litpro_users from 's3://sintl-data-analytics-07092019/litpro/users.csv' iam_role 'arn:aws:iam::209874575205:role/sintl-data-analytics-07092019-role' 
timeformat 'YYYY-MM-DD HH:MI:SS' escape removequotes delimiter as ',' IGNOREHEADER 1 ACCEPTINVCHARS COMPUPDATE OFF STATUPDATE OFF;

copy sintldata_stg.litpro_lf_classes from 's3://sintl-data-analytics-07092019/litpro/lf_classes.csv' iam_role 'arn:aws:iam::209874575205:role/sintl-data-analytics-07092019-role' 
timeformat 'YYYY-MM-DD HH:MI:SS' escape removequotes delimiter as ',' IGNOREHEADER 1 ACCEPTINVCHARS COMPUPDATE OFF STATUPDATE OFF;

