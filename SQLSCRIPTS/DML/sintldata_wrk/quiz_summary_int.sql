TRUNCATE TABLE sintldata_wrk.quiz_summary_int;

INSERT INTO sintldata_wrk.quiz_summary_int(
	SELECT 'Zone-4' as region
  ,quf.org_id
	,quf.school_id as id
	,quf.school_name
	,quf.year
	,quf.term
	,quf.grade
	,quf.pass_count
	,quf.fail_count
	,(quf.pass_count + quf.fail_count) AS quiz_count
	,quf.word_count 
	FROM 
	(
	SELECT qu.school_id
		,qu.year
		,qu.term
		,qu.grade
		,ur.organization_id
		,org.id as org_id
		,org.name as school_name
		,CASE qu.result
			WHEN 'passed'
				THEN count(qu.quiz_id)
			ELSE '0'
			END AS pass_count
		,CASE qu.result
			WHEN 'failed'
				THEN count(qu.quiz_id)
			ELSE '0'
			END AS fail_count
		,'' AS quiz_count
		,sum(qu.word_count) AS word_count
	FROM sintldata_wrk.organizations org
  Left JOIN sintldata_wrk.users ur ON ur.organization_id=org.id
  Left Join sintldata_wrk.quiz qu On qu.student_id=ur.user_id
  GROUP BY qu.school_id
	  ,qu.year
		,qu.term
		,ur.organization_id
		,qu.grade
		,qu.result
		,org.id
		,org.name
	)quf
	);
