TRUNCATE sintldata_wrk.users;
INSERT INTO sintldata_wrk.users (
	Select 'Zone-4' as region,
       ua.id,
       ua.first_name,
       ua.last_name ,
       ua.type,
       ua.organization_id,
       ua.deleted,
       ul.id as user_id,
       ul.school_id 
       
From sintldata_stg.slz_core_user_accounts ua
Join sintldata_stg.litpro_users ul ON ua.uuid=ul.slz_id
	);
