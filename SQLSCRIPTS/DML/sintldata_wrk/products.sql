TRUNCATE sintldata_wrk.products;
INSERT INTO sintldata_wrk.products (
	SELECT 'Zone-4' AS region
	,id AS product_id
	,code
	,description
	,display_name
	,deleted 
	FROM sintldata_stg.slz_core_products
	);