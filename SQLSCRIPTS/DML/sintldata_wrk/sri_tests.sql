TRUNCATE TABLE sintldata_wrk.sri_tests;
INSERT INTO sintldata_wrk.sri_tests (
	SELECT 'Zone-4' AS region
	,t.id AS test_id
	,t.user_id
	,t.grade_id
	,t.school_year AS year
	,t.created
	,t.time_start AS start
	,t.time_end AS finish FROM sintldata_stg.litpro_sri_issued_tests t
	,sintldata_stg.litpro_grades g WHERE t.grade_id = g.id
	AND t.test_state = 4
	);
