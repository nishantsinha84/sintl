TRUNCATE sintldata_wrk.scorecard_int;
INSERT INTO sintldata_wrk.scorecard_int
(
SELECT region AS region
	,CAST (year as INTEGER) AS year
	,id AS org_id
	,school_name AS school_name
	,lp_count_cur_yr AS lp_count_cur_yr
	,lp_count_pre_yr AS lp_count_pre_yr
	,lpl_count_cur_yr AS lpl_count_cur_yr
	,lpl_count_pre_yr AS lpl_count_pre_yr
	,total_licenses_cur_yr AS total_licenses_cur_yr
	,total_licenses_pre_yr AS total_licenses_pre_yr
	,school_size AS school_size
	,product_coverage AS product_coverage
	,continuity AS continuity
	,CAST(size_factor AS DECIMAL(38,2)) AS size_factor
	,CAST(continuity_factor AS DECIMAL(38,2)) AS continuity_factor
	,CAST(coverage_factor AS DECIMAL(38,2)) AS coverage_factor
	,CAST(rating AS DECIMAL(38,2)) AS rating
	,CASE 
		WHEN rating < 1.22
			THEN '-'
		WHEN rating >= 1.22
			AND rating < 1.61
			THEN 'Moderate Risk'
		ELSE 'High Risk'
		END AS scorecard
FROM (
	SELECT region AS region
		,year AS year
		,id AS id
		,school_name AS school_name
		,lp_count_cur_yr AS lp_count_cur_yr
		,lp_count_pre_yr AS lp_count_pre_yr
		,lpl_count_cur_yr AS lpl_count_cur_yr
		,lpl_count_pre_yr AS lpl_count_pre_yr
		,total_licenses_cur_yr AS total_licenses_cur_yr
		,total_licenses_pre_yr AS total_licenses_pre_yr
		,school_size AS school_size
		,product_coverage AS product_coverage
		,continuity AS continuity
		,CAST(size_factor AS DECIMAL(38,2)) AS size_factor
		,CAST(continuity_factor AS DECIMAL(38,2)) AS continuity_factor
		,CAST(coverage_factor AS DECIMAL(38,2)) AS coverage_factor
		,CAST((0.5 * size_factor) + (0.3 * continuity_factor) + (0.2 * coverage_factor) AS DECIMAL(38,2)) AS rating
		,'' AS scorecard
	FROM (
		SELECT region AS region
			,year AS year
			,id AS id
			,school_name AS school_name
			,lp_count_cur_yr AS lp_count_cur_yr
			,lp_count_pre_yr AS lp_count_pre_yr
			,lpl_count_cur_yr AS lpl_count_cur_yr
			,lpl_count_pre_yr AS lpl_count_pre_yr
			,total_licenses_cur_yr AS total_licenses_cur_yr
			,total_licenses_pre_yr AS total_licenses_pre_yr
			,school_size AS school_size
			,product_coverage AS product_coverage
			,continuity AS continuity
			,CASE 
				WHEN school_size = 'Small'
					THEN 1 / 0.5
				WHEN school_size = 'Medium'
					THEN 1 / 0.7
				ELSE 1
				END AS size_factor
			,CASE 
				WHEN continuity = 'Reducing'
					THEN 1 / 0.5
				WHEN continuity = 'Flat'
					THEN 1 / 0.7
				ELSE 1
				END AS continuity_factor
			,CASE 
				WHEN product_coverage = 'Narrow'
					THEN 1 / 0.5
				WHEN product_coverage = 'Moderate'
					THEN 1 / 0.7
				ELSE 1
				END AS coverage_factor
			,0 AS rating
			,'' AS scorecard
		FROM (
			SELECT region AS region
				,year AS year
				,id AS id
				,school_name AS school_name
				,lp_count_cur_yr AS lp_count_cur_yr
				,lp_count_pre_yr AS lp_count_pre_yr
				,lpl_count_cur_yr AS lpl_count_cur_yr
				,lpl_count_pre_yr AS lpl_count_pre_yr
				,total_licenses_cur_yr AS total_licenses_cur_yr
				,total_licenses_pre_yr AS total_licenses_pre_yr
				,CASE 
					WHEN total_licenses_cur_yr < 100
						THEN 'Small'
					WHEN total_licenses_cur_yr >= 400
						THEN 'Big'
					ELSE 'Medium'
					END AS school_size
				,CASE 
					WHEN (
							lp_count_cur_yr > 0
							AND lpl_count_cur_yr > 0
							)
						THEN 'Wide'
					WHEN lp_count_cur_yr > 0
						THEN 'Moderate'
					ELSE 'Narrow'
					END AS product_coverage
				,CASE 
					WHEN lp_count_cur_yr = lp_count_pre_yr
						THEN 'Flat'
					WHEN lp_count_cur_yr < lp_count_pre_yr
						THEN 'Reducing'
					ELSE 'Improvising'
					END AS continuity
				,0 AS size_factor
				,0 AS continuity_factor
				,0 AS coverage_factor
				,0 AS rating
				,'' AS scorecard
			FROM (
				SELECT 'Zone-4' AS region
					,substring(wrk_ord1.subscription_start_date, 1, 4) AS YEAR
					,wrk_org.id AS id
					,wrk_org.name AS school_name
					,isnull(wrk_ord1.license_count,0) AS lp_count_cur_yr
					,isnull(wrk_ord2.license_count,0) AS lp_count_pre_yr
					,isnull(wrk_ord3.license_count,0) AS lpl_count_cur_yr
					,isnull(wrk_ord4.license_count,0) AS lpl_count_pre_yr
					,(isnull(wrk_ord1.license_count,0)+ isnull(wrk_ord3.license_count,0)) AS total_licenses_cur_yr
					,(isnull(wrk_ord2.license_count,0) + isnull(wrk_ord4.license_count,0)) AS total_licenses_pre_yr
					,'' AS school_size
					,'' AS product_coverage
					,'' AS continuity
					,0 AS size_factor
					,0 AS continuity_factor
					,0 AS coverage_factor
					,0 AS rating
					,'' AS scorecard
				FROM sintldata_wrk.organizations wrk_org
				LEFT JOIN sintldata_wrk.order_details wrk_ord1 ON wrk_org.id = wrk_ord1.organization_id
					AND wrk_ord1.product_id = 44
					AND wrk_ord1.order_type='Normal'
					AND wrk_ord1.year = 2019
				LEFT JOIN sintldata_wrk.order_details wrk_ord2 ON wrk_org.id = wrk_ord2.organization_id
					AND wrk_ord2.product_id = 44
					AND wrk_ord2.order_type='Normal'
					AND wrk_ord2.year = 2018
				LEFT JOIN sintldata_wrk.order_details wrk_ord3 ON wrk_org.id = wrk_ord3.organization_id
					AND wrk_ord3.product_id = 46
					AND wrk_ord3.order_type='Normal'
					AND wrk_ord3.year = 2019
				LEFT JOIN sintldata_wrk.order_details wrk_ord4 ON wrk_org.id = wrk_ord4.organization_id
					AND wrk_ord4.product_id = 46
					AND wrk_ord4.order_type='Normal'
					AND wrk_ord4.year = 2018
				)
			)
		)
	)
);
