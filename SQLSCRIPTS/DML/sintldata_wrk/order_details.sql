
TRUNCATE sintldata_wrk.order_details;
INSERT INTO sintldata_wrk.order_details (
	SELECT 'Zone-4' AS region
	,oe.order_line_id
	,oe.organization_id
	,oe.product_id
	,oe.subscription_start_date
	,oe.subscription_end_date
	,substring(oe.subscription_start_date, 1, 4) AS year
	,oe.order_type
	,oe.license_count 
FROM sintldata_stg.slz_core_order_entitlements oe
	,sintldata_stg.slz_custportal_order_lines ol 
WHERE oe.order_line_id = ol.id
AND oe.product_id IN ('44','46')
AND ol.deleted = 'N'
);

