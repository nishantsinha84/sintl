Truncate table sintldata_wrk.quiz;
 INSERT INTO sintldata_wrk.quiz (
	SELECT qu.region
	,qu.quiz_id
	,qu.school_id
	,qu.student_id
	,qu.year
	,qu.quiz_date
	,CASE qu.month
		WHEN qu.month IN (
				1
				,2
				,3
				)
			THEN 'Term 1'
		WHEN qu.month IN (
				4
				,5
				,6
				)
			THEN 'Term 2'
		WHEN qu.month IN (
				7
				,8
				,9
				)
			THEN 'Term 3'
		WHEN qu.month IN (
				10
				,11
				,12
				)
			THEN 'Term 4'
		END AS term
	,qu.result
	,qu.word_count
	,qu.grade FROM (
	SELECT 'Zone-4' AS region
		,quiz.id AS quiz_id
		,quiz.school_id
		,quiz.student_id
		,quiz.school_year AS year
		,quiz.quiz_date
		,extract(month FROM quiz_date) AS month
		,CASE passed
			WHEN '1'
				THEN 'passed'
			WHEN '0'
				THEN 'failed'
			END AS result
		,quiz.word_count
		,'Grade-' + quiz.grade_code AS grade
	FROM sintldata_stg.litpro_reports_report_quiz_activity_for_school quiz
	) qu
	);
